#include "./MIMETypeMain.hpp"

int main()
{
    MimeTypes types;
    Filenames files;

    int N; // Number of elements which make up the association table.
    std::cin >> N; std::cin.ignore();
    int Q; // Number Q of file names to be analyzed.
    std::cin >> Q; std::cin.ignore();
    for (int i = 0; i < N; i++) {
        std::string EXT;     // file extension
        std::string MT;     // MIME type.
        std::cin >> EXT >> MT; std::cin.ignore();
        types.insert(EXT, MT);
    }
    for (int i = 0; i < Q; i++) {
        std::string FNAME;     // One file name per line.
        getline(std::cin, FNAME);
        files.insert(FNAME);
    }
    std::vector<std::string> mimetypes = types.getType(files);
    for ( std::string mimetype : mimetypes ) {
        std::cout << mimetype << std::endl;
    }
}
