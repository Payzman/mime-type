#include "./Filenames.hpp"

void Filenames::insert(std::string filename) {
    std::transform(filename.begin(), filename.end(), filename.begin(), ::tolower);
    unsigned long dot_position = filename.find_last_of(".");
    if ( dot_position == std::string::npos ) { // invalid filename
        this->filenames.push_back(" ");
        this->extensions.push_back(" ");
    } else {
        this->filenames.push_back(filename.substr(0, dot_position - 1));
        this->extensions.push_back(filename.substr(dot_position + 1));
    }
}

std::vector<std::string> Filenames::getExtensions() {
    return this->extensions;
}
