#include "./MimeTypes.hpp"

void MimeTypes::insert(std::string extension, std::string type) {
    std::transform(extension.begin(), extension.end(), extension.begin(), ::tolower);
    this->_types.insert({extension, type});
}

std::string MimeTypes::at(std::string extension) {
    return this->_types.at(extension);
}

std::vector<std::string> MimeTypes::getType(Filenames files) {
    std::vector<std::string> mimetypes;
    for ( std::string extension : files.getExtensions() ) {
        std::unordered_map<std::string, std::string>::const_iterator elem = this->_types.find(extension);
        if ( elem == this->_types.end() ) {
            mimetypes.push_back("UNKNOWN");
        } else {
            mimetypes.push_back(elem->second);
        }
    }
    return mimetypes;
}
