#ifndef MIMETYPES_H_
#define MIMETYPES_H_

#include <string>
#include <unordered_map>

#include "./Filenames.hpp"

class MimeTypes {
    private:
        std::unordered_map<std::string, std::string> _types;
    public:
        void insert(std::string, std::string);
        std::string at(std::string);
        std::vector<std::string> getType(Filenames);
};

#endif // MIMETYPES_H_
