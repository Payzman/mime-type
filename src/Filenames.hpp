#ifndef FILENAMES_H_
#define FILENAMES_H_

#include <algorithm>
#include <string>
#include <vector>

class Filenames {
    private:
        std::vector<std::string> filenames;
        std::vector<std::string> extensions;
    public:
        void insert(std::string);
        std::vector<std::string> getExtensions();
};

#endif // FILENAMES_H_
