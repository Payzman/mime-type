#include "./MIMETypeTest.hpp"

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wglobal-constructors"

TEST(MimeType, CreateMap) {
    std::string first_extension = "html";
    std::string first_type = "text/html";
    std::string second_extension = "png";
    std::string second_type = "image/png";
    std::string third_extension = "gif";
    std::string third_type = "image/gif";
    MimeTypes m;
    m.insert(first_extension, first_type);
    m.insert(second_extension, second_type);
    m.insert(third_extension, third_type);
    ASSERT_EQ(m.at(first_extension), first_type);
    ASSERT_EQ(m.at(second_extension), second_type);
    ASSERT_EQ(m.at(third_extension), third_type);
}

TEST(Filenames, insert) {
    std::string first = "animated.gif";
    std::string second = "portrait.png";
    std::string third = "index.html";
    Filenames f;
    f.insert(first);
    f.insert(second);
    f.insert(third);
    std::vector<std::string> extensions = f.getExtensions();
    ASSERT_EQ(extensions[0], "gif");
    ASSERT_EQ(extensions[1], "png");
    ASSERT_EQ(extensions[2], "html");
}

TEST(MimeType, getType) {
    std::string first_extension = "html";
    std::string first_type = "text/html";
    std::string second_extension = "png";
    std::string second_type = "image/png";
    std::string third_extension = "gif";
    std::string third_type = "image/gif";
    MimeTypes mimetypes;
    mimetypes.insert(first_extension, first_type);
    mimetypes.insert(second_extension, second_type);
    mimetypes.insert(third_extension, third_type);

    std::string first = "animated.gif";
    std::string second = "portrait.png";
    std::string third = "index.html";
    Filenames filenames;
    filenames.insert(first);
    filenames.insert(second);
    filenames.insert(third);

    std::vector<std::string> str_mimetypes = mimetypes.getType(filenames);

    ASSERT_EQ(str_mimetypes[0], "image/gif");
    ASSERT_EQ(str_mimetypes[1], "image/png");
    ASSERT_EQ(str_mimetypes[2], "text/html");
}

TEST(MimeType, CorrectDivision) {
    MimeTypes mimetypes;
    mimetypes.insert("pdf", "application/pdf");

    Filenames filenames;
    filenames.insert("pdf");

    std::vector<std::string> str_mimetypes = mimetypes.getType(filenames);

    ASSERT_EQ(str_mimetypes[0], "UNKNOWN");
}

TEST(MimeType, DoubleDots) {
    MimeTypes mimetypes;
    mimetypes.insert("pdf", "application/pdf");

    Filenames filenames;
    filenames.insert("report..pdf");

    std::vector<std::string> str_mimetypes = mimetypes.getType(filenames);

    ASSERT_EQ(str_mimetypes[0], "application/pdf");
}

TEST(MimeType, Cases) {
    MimeTypes mimetypes;
    mimetypes.insert("pDf", "application/PDF");
    Filenames filenames;
    filenames.insert("blaBla.Pdf");
    std::vector<std::string> str_mimetypes = mimetypes.getType(filenames);
    ASSERT_EQ(str_mimetypes[0], "application/PDF");
}
#pragma clang diagnostic pop
